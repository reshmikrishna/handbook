---
title: "Common Jetbrains Setup and Configuration"
no_list: true
---

## Overview

Jetbrains IDEs are standardized, so much of the setup and configuration information applies to all IDEs, and is consolidated on this page.

For setup and configuration and other info that is specific to individual IDEs, see the following pages:

- [GoLand](../goland)
- [RubyMine](../rubymine)
- [WebStorm](../webstorm)

## Resources

See the following resources for details on how to configure and setup JetBrains IDEs.

These resources will be migrated and expanded to this page and sub-pages page soon™️.

- [Chad Woolley's Jetbrains IDE setup notes](https://gitlab.com/cwoolley-gitlab/cwoolley-gitlab/-/blob/main/gitlab-workstation-setup-notes.md#jetbrains-ide-setup)
- [Chad Woolley's curated list of Jetbrains overridden settings](https://github.com/thewoolleyman/workstation/blob/master/README.md#jetbrains-overridden-settings)
